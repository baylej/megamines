// background color
var bgcol = makecol(182, 182, 182);

// one spritesheet fit-all
var spritesheet;

// width and height of a sprite element in pixels
var sprite_w = 7;
var sprite_h = 7;

// coordinates of sprites on the spritesheet in pixels
var pos_1 = [ 0,  0]; // see 1 mine
var pos_2 = [ 7,  0]; // see 2 mines
var pos_3 = [ 0,  7]; // see ...
var pos_4 = [ 7,  7];
var pos_5 = [ 0, 14];
var pos_6 = [ 7, 14];
var pos_7 = [ 0, 21];
var pos_8 = [ 7, 21];

var pos_grd  = [14,  0]; // ground
var pos_flag = [21,  0]; // flag
var pos_mine = [14,  7]; // mine (too bad)
var pos_void = [21,  7]; // void, nothing
var pos_new  = [14, 14]; // new game button
var pos_help = [21, 14]; // help button
var pos_ok   = [14, 21]; // not dead yet
var pos_fail = [21, 21]; // dead

// game data
var field_w, field_h; // width and height of the minefield in cell
var x_offset; // to center the minefield
var minefield = []; // array [field_w*field_h]
var minecount; // how many mines
var to_discover; // how many cell to discover

// const values of mineland[...]:
var MINE = 0x20;
var VOID = 0;
//  ONE  = 1;
//  TWO  = 2; ...

// flag set if the cell has not been discovered yet
var UNDISC_FLAG = 0x80;
// player flagged that cell
var FLAGGED_FLAG = 0x100;

// status of the game
var OK = 1;
var WIN = 2;
var FAIL = 0;
var gamestatus;

// draws the sprite at `pos` at the given cell coordinates
function cell(pos_array2, cell_x, cell_y) {
	stretch_blit(spritesheet, canvas,
	  pos_array2[0],               pos_array2[1],
	  sprite_w,                    sprite_h,
	  x_offset+cell_x*sprite_w,    cell_y*sprite_h + 3*sprite_h,
	  sprite_w,                    sprite_h);
}

// returns the value of the given cell
function what(cell_x, cell_y) {
	return minefield[cell_x + cell_y*field_w];
}

// returns the value of the given cell
function put(cell_x, cell_y, value) {
	minefield[cell_x + cell_y*field_w] = value;
}

// minefield[x][y] ^= flag
function toggle(cell_x, cell_y, flag) {
	put(cell_x, cell_y, what(cell_x, cell_y) ^ flag);
}

// returns an array of neighbours cell coordinates (int[?][2])
function neighbours(cell_x, cell_y) {
	var res = [];

	if (cell_x>0) {
		res.push([cell_x-1, cell_y]);
		if (cell_y>0) res.push([cell_x-1, cell_y-1]);
		if (cell_y<field_h-1) res.push([cell_x-1, cell_y+1]);
	}
	if (cell_x<field_w-1) {
		res.push([cell_x+1, cell_y]);
		if (cell_y>0) res.push([cell_x+1, cell_y-1]);
		if (cell_y<field_h-1) res.push([cell_x+1, cell_y+1]);
	}

	if (cell_y>0) res.push([cell_x, cell_y-1]);
	if (cell_y<field_h-1) res.push([cell_x, cell_y+1]);

	return res;
}

// discovers the giver cell
function discover(cell_x, cell_y) {
	var cellv = what(cell_x, cell_y);

	if (!(cellv & FLAGGED_FLAG) && cellv & UNDISC_FLAG) {
		toggle(cell_x, cell_y, UNDISC_FLAG);
		// Uuups -_-
		if (cellv & MINE) {
			gamestatus = FAIL;
		}
		else {
			to_discover--;
			console.log("cellv=" + cellv);
			if ((cellv & 0x0F) == VOID) {
				console.log("propagating...");
				// propogation algorithm
				var neigh = neighbours(cell_x, cell_y);
				for (var it=0; it<neigh.length; it++) {
					discover(neigh[it][0], neigh[it][1]);
				}
			}
		}
	}
}

// returns how many mines are seen from sentinel at given cell
function how_many(cell_x, cell_y) {
	var res = 0;
	var neigh = neighbours(cell_x, cell_y);
	for (var it=0; it<neigh.length; it++) {
		if (what(neigh[it][0], neigh[it][1]) & MINE) res++;
	}
	return res;
}

// returns `true` if the mouse is over the minefield
function mouse_ison_minefield() {
	return mouse_x > x_offset && mouse_x < SCREEN_W-x_offset &&
	  mouse_y > 3*sprite_h && mouse_y < (field_h+3)*sprite_h;
}

// returns `true` if the mouse is over the newgame button
function mouse_ison_newgame() {
	return mouse_x > x_offset && mouse_x < x_offset+sprite_w*3 &&
	  mouse_y > 0 && mouse_y < 3*sprite_h;
}

// returns cell coordinates at current  an int[2]
function mouse_to_cell() {
	var res = [];
	res[0] = Math.floor((mouse_x -   x_offset) / sprite_w);
	res[1] = Math.floor((mouse_y - 3*sprite_h) / sprite_h);
	return res;
}

function generate_minefield() {
	field_w = Math.floor(SCREEN_W / sprite_w);
	x_offset = (SCREEN_W - field_w*sprite_w) / 2;
	field_h = Math.floor((SCREEN_H - 3*sprite_h) / sprite_h);
	minecount = 0;
	gamestatus = OK;

	// randomise mines
	for (var it=0; it<field_w*field_h; it++) {
		minefield[it] = (rand() < 8000) ? MINE: VOID;
		if (minefield[it] == MINE) minecount++;
	}

	to_discover = field_w * field_h - minecount;

	// set sentinels
	for (var xi=0; xi<field_w; xi++) {
		for (var yi=0; yi<field_h; yi++) {

			// if is not a mine, may be a sentinel
			if (!(what(xi, yi) & MINE)) {
				var see = how_many(xi, yi);
				if (see > 0) {
					put(xi, yi, see);
				}
			}

			// set the undiscovered flag
			put(xi, yi, what(xi, yi) | UNDISC_FLAG);
		}
	}
}

function redraw() {
	// draw toolbar
	rectfill(canvas, 0, 0, SCREEN_W, 3*sprite_h, bgcol);

	// Smiley face
	var smiley = (gamestatus)? pos_ok: pos_fail;
	stretch_blit (spritesheet, canvas,
	  smiley[0], smiley[1],
	  sprite_w,  sprite_h,
	  SCREEN_W/2. - sprite_w*3/2, 0,
	  sprite_w*3,  sprite_h*3);

	// Reset
	stretch_blit (spritesheet, canvas,
	  pos_new[0], pos_new[1],
	  sprite_w,   sprite_h,
	  x_offset,   0,
	  sprite_w*3, sprite_h*3);

	// Help
	stretch_blit (spritesheet, canvas,
	  pos_help[0], pos_help[1],
	  sprite_w,    sprite_h,
	  x_offset+sprite_w*4, 0,
	  sprite_w*3,  sprite_h*3);

	textout(canvas, font, ""+to_discover,
	  sprite_w*8, sprite_w*3, 20, makecol(0, 0, 0));

	// draw minefield
	for (var xi=0; xi<field_w; xi++) {
		for (var yi=0; yi<field_h; yi++) {

			var cell_v = what(xi, yi);

			if (cell_v & UNDISC_FLAG) {
				if (cell_v & FLAGGED_FLAG) {
					cell(pos_flag, xi, yi);
				}
				else {
					cell(pos_grd,  xi, yi);
				}
			}
			else {
				switch (cell_v & ~UNDISC_FLAG) {
					case MINE: cell(pos_mine, xi, yi); break;
					case VOID: cell(pos_void, xi, yi); break;

					case 1: cell(pos_1, xi, yi); break;
					case 2: cell(pos_2, xi, yi); break;
					case 3: cell(pos_3, xi, yi); break;
					case 4: cell(pos_4, xi, yi); break;
					case 5: cell(pos_5, xi, yi); break;
					case 6: cell(pos_6, xi, yi); break;
					case 7: cell(pos_7, xi, yi); break;
					case 8: cell(pos_8, xi, yi); break;

					default: cell(pos_help,  xi, yi); break;
				}
			}
		}
	}

	if (gamestatus == WIN) {
		textout_centre(canvas, font, "GG",
		  SCREEN_W/2, SCREEN_H/2, 48, makecol(0, 0, 0));
	}
}

function logic() {
	if (mouse_ison_minefield() && gamestatus == OK) {
		var cellp = mouse_to_cell();
		console.log("clic on cell (" + cellp[0] + ", " + cellp[1] + ")");
		// right clic: flag current cell
		if (mouse_b & 4 && what(cellp[0], cellp[1]) & UNDISC_FLAG) {
			toggle(cellp[0], cellp[1], FLAGGED_FLAG);
		}
		// left clic: discover current cell
		if (mouse_b & 1) {
			discover(cellp[0], cellp[1]);
		}

		if (to_discover == 0) {
			gamestatus = WIN;
		}
	}
	else {
		if (mouse_ison_newgame()) {
			generate_minefield();
		}
	}
}

// Main: initialises, loads, sets, ...
function main() {
	allegro_init();
	set_gfx_mode("canvas_id", null, null, false);
	install_mouse();

	spritesheet = load_bmp("spritesheet.png");

	generate_minefield();

	ready(function(){
		clear_to_color(canvas, bgcol);
		mouse_pressed = 1; // tricky ;-) may break in future ver. of allegro.js

		loop(function(){

			if (mouse_pressed) {
				logic();
				redraw();
			}

		}, BPS_TO_TIMER(60)); // 60 frame per seconds
	});
}
END_OF_MAIN();
